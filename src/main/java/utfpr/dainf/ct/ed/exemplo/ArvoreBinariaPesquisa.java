package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Exemplo de implementação de árvore binária de pesquisa.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {

    protected ArvoreBinariaPesquisa<E> pai;

    boolean isExcluido = false;
    
    protected void excluirNo(ArvoreBinariaPesquisa<E> no){
        no.direita = null;
        no.esquerda = null;
        no.pai = null;
        
        no.isExcluido = true;
    }
    
    //espero q de boa
    /*protected ArvoreBinariaPesquisa<E> esquerda;
    protected ArvoreBinariaPesquisa<E> direita;*/

    /*@Override
    public ArvoreBinariaPesquisa<E> getEsquerda() {
        return esquerda;
    }
    
    public void setEsquerda(ArvoreBinariaPesquisa<E> esquerda) {
        this.esquerda = esquerda;
    }
    
    @Override
    public ArvoreBinariaPesquisa<E> getDireita() {
        return direita;
    }

    public void setDireita(ArvoreBinariaPesquisa<E> direita) {
        this.direita = direita;
    }*/

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     *
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     *
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     *
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     *
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        if (valor == this.valor) {
            return this;
        } else if (valor.compareTo(this.valor) < 0) {
            if (this.esquerda == null) {
                return null;
                //}else return ((ArvoreBinariaPesquisa<E>)this.esquerda).pesquisa(valor);
            } else {
                return ((ArvoreBinariaPesquisa<E>)this.esquerda).pesquisa(valor);
            }
        } else {
            if (this.direita == null) {
                return null;
                //}else return ((ArvoreBinariaPesquisa<E>)this.direita).pesquisa(valor);
            } else {
                return ((ArvoreBinariaPesquisa<E>)this.direita).pesquisa(valor);
            }
        }
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     *
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() {
        ArvoreBinariaPesquisa<E> no = this;
        while (no != null && no.esquerda != null) {
            //no = (ArvoreBinariaPesquisa<E>)no.esquerda;
            no = ((ArvoreBinariaPesquisa<E>)no.esquerda);
        }
        return no;
    }

    /**
     * Retorna o nó da árvore com o maior valor.
     *
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<E> no = this;
        while (no != null && no.direita != null) {
            //no = (ArvoreBinariaPesquisa<E>)no.direita;
            no = ((ArvoreBinariaPesquisa<E>)no.direita);
        }
        return no;
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     *
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {
     * @null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        if (no != null && no.direita != null) {
            return ((ArvoreBinariaPesquisa<E>)no.direita).getMinimo();
        }

        ArvoreBinariaPesquisa<E> pai = no.pai;

        while (pai != null && no.equals(pai.direita)) {
            no = pai;
            pai = pai.pai;
        }
        return pai;
    }

    /**
     * Retorna o nó predecessor do nó especificado.
     *
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {
     * @null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        if (no != null && no.esquerda != null) {
            return ((ArvoreBinariaPesquisa<E>)no.esquerda).getMaximo();
        }

        ArvoreBinariaPesquisa<E> pai = no.pai;

        while (pai != null && no.equals(pai.esquerda)) {
            no = pai;
            pai = pai.pai;
        }
        return pai;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     *
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
        if (valor.compareTo(this.valor) < 0) {
            if (this.esquerda == null) {
                ArvoreBinariaPesquisa<E> retorno
                        = new ArvoreBinariaPesquisa<>(valor);
                this.esquerda = retorno;
                retorno.setPai(this);
                return retorno;
            } else {
                return ((ArvoreBinariaPesquisa<E>)this.esquerda).insere(valor);
            }
        } else {
            if (this.direita == null) {
                ArvoreBinariaPesquisa<E> retorno
                        = new ArvoreBinariaPesquisa<>(valor);
                this.direita = retorno;
                retorno.setPai(this);
                return retorno;
            } else {
                return ((ArvoreBinariaPesquisa<E>)this.direita).insere(valor);
            }
        }
    }

    /**
     * Exclui o nó especificado da árvore. Se a raiz for excluída, retorna a
     * nova raiz.
     *
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no){
               
        ArvoreBinariaPesquisa<E> raiz = this;

        //encontrando a raiz
        while (raiz.pai != null) {
            raiz = raiz.pai;
        }

        /*if(no.isExcluido){
            return raiz;
        }*/
        
        no = raiz.pesquisa(no.valor);
        
        //sem filhos
        if (no.esquerda == null && no.direita == null) {
            //excluindo o no no caso dele ser o da esquerda do pai
            if(no.equals(raiz)){
                excluirNo(no);
                no.valor = null;
                return null;
            }
            if (no.equals(no.pai.esquerda)) {
                no.pai.esquerda = null;
            } //excluindo o no no caso dele ser o da esquerda do pai
            else if (no.equals(no.pai.direita)) {
                no.pai.direita = null;
            }
            excluirNo(no);
        } //1 filho
        else if ((no.esquerda == null && no.direita != null)
                || no.esquerda != null && no.direita == null) {
            //existe um filho na esquerda
            if (no.esquerda != null) {

                //filho aponta para o avô
                ((ArvoreBinariaPesquisa<E>)no.esquerda).pai = no.pai;

                //pai aponta pro neto esquerdo do filho esquerdo
                if (no.equals(no.pai.esquerda)) {
                    no.pai.esquerda = no.esquerda;
                } //excluindo o no no caso dele ser o da esquerda do pai
                //pai aponta pro neto esquerdo do filho direito
                else if (no.equals(no.pai.direita)) {
                    no.pai.direita = no.esquerda;
                }
            } //existe um filho na direita
            else if (no.direita != null) {

                //filho aponta para o avô
                ((ArvoreBinariaPesquisa<E>)no.direita).pai = no.pai;

                //pai aponta pro neto direito do filho esquerdo
                if (no.equals(no.pai.esquerda)) {
                    no.pai.esquerda = no.direita;
                } //pai aponta pro neto direito do filho direito
                else if (no.equals(no.pai.direita)) {
                    no.pai.direita = no.direita;
                }
            }
            excluirNo(no);
        } //2 filhos
        else if (no.direita != null && no.esquerda != null){
            ArvoreBinariaPesquisa<E> sucessorCom1Filho;
            
            sucessorCom1Filho = no.sucessor(no);
            
            //enquanto sucessor tem 2 filhos
            while(sucessorCom1Filho.esquerda != null &&
                    sucessorCom1Filho.direita != null){
                sucessorCom1Filho = no.sucessor(sucessorCom1Filho);
            }
            
            E valor = sucessorCom1Filho.valor;
            no.exclui(sucessorCom1Filho);
            no.valor = valor;
        }

        return raiz;        
    }
    
    /**public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> raiz = this;

        //encontrando a raiz
        while (raiz.pai != null) {
            raiz = raiz.pai;
        }        
        
        if(no.esquerda == null || no.direita == null){
            no.pai = no;
        }else no.pai = no.sucessor(no);
        
        ArvoreBinariaPesquisa<E> aux;
        
        if(no.pai.esquerda != null){
            aux = (ArvoreBinariaPesquisa<E>)no.pai.esquerda;
        }else{
            aux = (ArvoreBinariaPesquisa<E>)no.pai.direita;
        }
        
        if(aux!=null){
            aux.pai = no.pai.pai;
        }
        if(no.pai.pai ==  null){
            raiz = aux;
        }else {
            if(no == no.pai.pai.esquerda){
                no.pai.pai.esquerda = aux;
            }else {
                no.pai.pai.direita = aux;
            }
        }
        if(pai != no){
            no.valor = pai.valor;
        }
        return raiz;
    }*/
}
